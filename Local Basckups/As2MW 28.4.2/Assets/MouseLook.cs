﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float mouseSensitivityX;
    public float mouseSensitivityY;

    float xAxisClamp = 0;

    public Transform playerParent;

    bool holdingObject = false;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

    }

    void Update()
    {
        RotateCamera();
        Debug.DrawRay(transform.position, transform.forward * 1.75f, Color.blue);

        if(!holdingObject) {
        if (Input.GetKeyDown(KeyCode.E)) { GrabObject();  holdingObject = true; }

        } else {
            HoldObject(grabbedObject);

        }
        

    }

    void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotateAmountX = mouseX * mouseSensitivityX;
        float rotateAmountY = mouseY * mouseSensitivityY;

        Vector3 targetRotateCam = transform.rotation.eulerAngles;
        Vector3 targetRotateBody = playerParent.rotation.eulerAngles;

        targetRotateCam.x -= rotateAmountY;
        targetRotateCam.z = 0;
        targetRotateBody.y += rotateAmountX;

        xAxisClamp -= rotateAmountY;

        if(xAxisClamp > 80) {
            xAxisClamp = 80;
            targetRotateCam.x = 80;

        } else if(xAxisClamp < -80) {
            xAxisClamp = -80;
            targetRotateCam.x = -80;

        }

        //transform.rotation is in quaternions | Quaternions.Euler() converts to vector3
        transform.rotation = Quaternion.Euler(targetRotateCam);
        playerParent.rotation = Quaternion.Euler(targetRotateBody);

        if (Input.GetKeyDown("escape")) { 
            Cursor.lockState = CursorLockMode.None;
        }
    }

    Transform grabbedObject;
    RaycastHit objectGrabRay;

    void GrabObject()
    {
        if(Physics.Raycast(transform.position, transform.forward * 1.75f, out objectGrabRay, 2f))
        {
            grabbedObject = objectGrabRay.transform;
   
        }
    }

    void HoldObject(Transform grabbedObject)
    {
        grabbedObject.transform.position = transform.position;
    }
}
    /*
    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;

    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis

    void Start()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }
    */
    /*

    Vector2 mouseLook;
    Vector2 smoothV;
    public float mouseSensitivity = 5.0f;
    public float smoothing = 2.0f;

    GameObject character;

    void Start()
    {
        
    }
    */


