﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour {

    float moveAcceleration = 10.0F;
    public float maxMovementSpeed = 5.0f;
    public float jumpForce = 2.0F;
    public float groundDrag = 0.0f;
    public float airDrag = 0.0f;
    public float groundAcceleration = 70.0f;
    public float airAcceleration = 20.0f;

    public float fallMultiplyier = 2.5f;

    public bool isGrounded = true;

    Rigidbody myRigidbody;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        

        if (!isGrounded) {
            if (myRigidbody.velocity.y < 0)
            {
                myRigidbody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplyier - 1) * Time.deltaTime;
            }
        }

        myRigidbody.velocity = Vector3.ClampMagnitude(myRigidbody.velocity, maxMovementSpeed);
        print(moveAcceleration);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();


        }
    }


    void FixedUpdate()
    {


        if (isGrounded)
        {
            moveAcceleration = groundAcceleration;
        } else
        {
            moveAcceleration = airAcceleration;
            WaitForLanding();
           
        }
        
            if (Input.GetKey(KeyCode.W))
            {
                myRigidbody.AddForce(transform.forward * moveAcceleration);

            }
            if (Input.GetKey(KeyCode.A))
            {
                myRigidbody.AddForce(-transform.right * moveAcceleration);

            }
            if (Input.GetKey(KeyCode.S))
            {
                myRigidbody.AddForce(-transform.forward * moveAcceleration);

            }
            if (Input.GetKey(KeyCode.D))
            {
                myRigidbody.AddForce(transform.right * moveAcceleration);

            }

}

    void Jump()
    {
        if(Physics.Raycast(transform.position, -transform.up, 1.75f))
        {
            isGrounded = false;
            Debug.DrawRay(transform.position, -transform.up * 1.75f, Color.red);
            myRigidbody.drag = airDrag;
            myRigidbody.AddForce(transform.up * jumpForce, ForceMode.Impulse);
        } 
    }
    void WaitForLanding()
    {
        Debug.DrawRay(transform.position, -transform.up * 1.75f, Color.green);
        if (Physics.Raycast(transform.position, -transform.up, 1.8f))
        {
            isGrounded = true;
            myRigidbody.drag = groundDrag;
        }
    }

    /*
    void OnCollisionStay(Collision collision)
    {
        isGrounded = true;
        myRigidbody.drag = groundDrag;
    }
    private void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
        myRigidbody.drag = airDrag;
    }
    */
    /*
 CharacterController characterController;
 public float movemoveAcceleration;

 void Awake()
 {

     characterController = GetComponent<CharacterController>();

 }

 void Update()
 {
     MovePlayer();

 }

 void MovePlayer()
 {
     float horizonalInput = Input.GetAxis("Horizontal");
     float verticalInput = Input.GetAxis("Vertical");

     Vector3 moveDirectionSidewards = transform.right * horizonalInput * movemoveAcceleration;
     Vector3 moveDirectionForward = transform.forward * verticalInput * movemoveAcceleration;

     characterController.SimpleMove(moveDirectionSidewards);
     characterController.SimpleMove(moveDirectionForward);

 }
 */

}

/*
public float moveAcceleration = 10.0F;

void Start()
{
    Cursor.lockState = CursorLockMode.Locked;
}

void Update()
{
    float translation = Input.GetAxis("Vertical") * moveAcceleration;
    float straffe = Input.GetAxis("Horizontal") * moveAcceleration;
    translation *= Time.deltaTime;
    straffe *= Time.deltaTime;

    transform.Translate(straffe, 0, translation);

    if (Input.GetKeyDown("escape"))
        Cursor.lockState = CursorLockMode.None;
}
*/
/*
public float mouseSensitivity = 100.0f;
public float clampAngle = 80.0f;

private float rotY = 0.0f; // rotation around the up/y axis
private float rotX = 0.0f; // rotation around the right/x axis

void Start()
{
    Vector3 rot = transform.localRotation.eulerAngles;
    rotY = rot.y;
    rotX = rot.x;
}

void Update()
{
    float mouseX = Input.GetAxis("Mouse X");
    float mouseY = -Input.GetAxis("Mouse Y");

    rotY += mouseX * mouseSensitivity * Time.deltaTime;
    rotX += mouseY * mouseSensitivity * Time.deltaTime;

    rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

    Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
    transform.rotation = localRotation;
}
 */

