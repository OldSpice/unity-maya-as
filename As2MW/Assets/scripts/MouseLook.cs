﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float mouseSensitivityX;
    public float mouseSensitivityY;

    float inventoryWeight  = 0;

    float xAxisClamp = 0;

    public Transform playerParent;
    public Transform holdingAnchor;

    bool holdingObject = false;

 

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

    }

    float startTime = 0;
    float heldTime;
    bool tryGrab = false;

    void Update()
    {
        RotateCamera();
        Debug.DrawRay(transform.position, transform.forward * 1.75f, Color.blue);

        if (!holdingObject)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                grabbedObject = holdingAnchor;
                tryGrab = true;
                startTime = Time.time;

            }
            else if (tryGrab && Input.GetKeyUp(KeyCode.E))
            {
                heldTime = Time.time - startTime;

                if (tryGrab && heldTime < 0.3f)
                {
                    GrabObject(false);
                    TakeObject(grabbedObject);
                    tryGrab = false;
                }
                else if (tryGrab)
                {
                    GrabObject(true);
                    tryGrab = false;
                }
            }
        } else {
            
            HoldObject(grabbedObject);

        }

        print(holdingObject);

        if (Input.GetKeyDown(KeyCode.Tab)) {
            DropObject();
        }

    }

    void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotateAmountX = mouseX * mouseSensitivityX;
        float rotateAmountY = mouseY * mouseSensitivityY;

        Vector3 targetRotateCam = transform.rotation.eulerAngles;
        Vector3 targetRotateBody = playerParent.rotation.eulerAngles;

        targetRotateCam.x -= rotateAmountY;
        targetRotateCam.z = 0;
        targetRotateBody.y += rotateAmountX;

        xAxisClamp -= rotateAmountY;

        if(xAxisClamp > 80) {
            xAxisClamp = 80;
            targetRotateCam.x = 80;

        } else if(xAxisClamp < -80) {
            xAxisClamp = -80;
            targetRotateCam.x = -80;

        }

        transform.rotation = Quaternion.Euler(targetRotateCam);
        playerParent.rotation = Quaternion.Euler(targetRotateBody);

        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }

    Transform grabbedObject;
    RaycastHit objectGrabRay;

    void GrabObject(bool toHold)
    {


        if (Physics.Raycast(transform.position, transform.forward * 1.75f, out objectGrabRay, 2f))
        {
            grabbedObject = objectGrabRay.transform;
            holdingObject = toHold;
        }
                

    }

    void HoldObject(Transform grabbedObject)
    {
        try
        {
            if (grabbedObject.GetComponent<objectCarryInformation>().canHold)
            {

                grabbedObject.transform.position = Vector3.Slerp(grabbedObject.position, transform.position + transform.forward * 2, Time.deltaTime * 8);
                grabbedObject.GetComponent<Rigidbody>().useGravity = false;
                print(grabbedObject.GetComponent<objectCarryInformation>().ItemName());
                print(grabbedObject.GetComponent<objectCarryInformation>().ItemName().Equals("testCube"));
            }
        } catch (NullReferenceException e)
        {
            holdingObject = false;
            print("ERROR");
        }
        if (Input.GetMouseButtonDown(0)) {
            Vector3 keepVelocity = grabbedObject.GetComponent<Rigidbody>().velocity;
            grabbedObject.GetComponent<Rigidbody>().useGravity = true;
                //grabbedObject.GetComponent<Rigidbody>().velocity += keepVelocity;
            grabbedObject.GetComponent<Rigidbody>().AddForce(10 * transform.forward, ForceMode.Impulse);
            grabbedObject = holdingAnchor;
            holdingObject = false;
        } else if (Input.GetKeyDown(KeyCode.X)) {
            Vector3 keepVelocity = grabbedObject.GetComponent<Rigidbody>().velocity;
            grabbedObject.GetComponent<Rigidbody>().useGravity = true;
            //grabbedObject.GetComponent<Rigidbody>().velocity += keepVelocity;
            grabbedObject = holdingAnchor;
            holdingObject = false;
        }
    }

    void TakeObject(Transform grabbedObject)
    {
        if (grabbedObject.GetComponent<objectCarryInformation>().canTake) {
            try
            {

                inventoryWeight += grabbedObject.GetComponent<objectCarryInformation>().GetWeight();
                addItemToInventory(grabbedObject);
                grabbedObject.gameObject.SetActive(false);
                grabbedObject = holdingAnchor;
                holdingObject = false;
                print(inventoryWeight);
            }
            catch (NullReferenceException e)
            {
                holdingObject = false;
                print("ERROR");
            }
        }
    }

    Transform[] inventoryItems = new Transform[50];
    int heldItems = 0;
    public int testCubes = 0;
    public int returnCubes()
    {
        return testCubes;
    }
    void addItemToInventory(Transform grabbedObject)
    {
        inventoryItems[heldItems] = grabbedObject;
        print(inventoryItems[heldItems]);
        testCubes++;
        heldItems++;
        print(heldItems);
    }

    void DropObject()
    {
        try
        {
            for (int i = 0; i < inventoryItems.Length; i++)
            {
                if (inventoryItems[i].GetComponent<objectCarryInformation>().ItemName().Equals("Apple"))
                {
                    inventoryItems[i].gameObject.SetActive(true);
                    inventoryItems[i].position = transform.position + transform.forward * 2;
                    testCubes--;
                    inventoryItems[i] = holdingAnchor;
                    break;
                }
                if (inventoryItems[i].GetComponent<objectCarryInformation>().ItemName().Equals("Curcuit Board"))
                {
                    inventoryItems[i].gameObject.SetActive(true);
                    inventoryItems[i].position = transform.position + transform.forward * 2;
                    inventoryItems[i] = holdingAnchor;
                    break;
                }
            }
        }
        catch (NullReferenceException e)
        {
            holdingObject = false;
            print("ERROR");
        }


    }
}




