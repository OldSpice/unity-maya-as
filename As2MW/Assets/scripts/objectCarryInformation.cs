﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectCarryInformation : MonoBehaviour {

    public float weight;
    public bool canTake;
    public bool canHold;
    public string itemName;

    public float GetWeight() {
        return weight;
    }

    public string ItemName()
    {
        return itemName;
    }
}
