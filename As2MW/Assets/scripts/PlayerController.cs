﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float walkSpeed;
    public float runSpeed;
    float movementSeed;
    public float jumpForce;

    public float maxSpeed = 10;

    Rigidbody myRigidbody;

    bool isGrounded = true;

    public float fallMultiplyier = 2.5f;

    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }


    private void Update()
    {


        if (Input.GetKey(KeyCode.LeftShift))
        {
            movementSeed = runSpeed;
        } else
        {
            movementSeed = walkSpeed;
        }

        if (Physics.Raycast(transform.position, -transform.up, 1.75f))
        {
            isGrounded = true;
        } else
        {
            isGrounded = false;
        }
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }
        if (!isGrounded)
        {
            if (myRigidbody.velocity.y < 0)
            {
                myRigidbody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplyier - 1) * Time.deltaTime;
            }
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            myRigidbody.AddForce(transform.forward * movementSeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            myRigidbody.AddForce(-transform.right * movementSeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            myRigidbody.AddForce(-transform.forward * movementSeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            myRigidbody.AddForce(transform.right * movementSeed);
        }
        if (myRigidbody.velocity.magnitude > maxSpeed)
        {
            myRigidbody.velocity = myRigidbody.velocity.normalized * maxSpeed;
        }
        if (!Input.GetKey(KeyCode.W) || !Input.GetKey(KeyCode.A) || !Input.GetKey(KeyCode.S) || !Input.GetKey(KeyCode.D))
        {
            myRigidbody.velocity = new Vector3(myRigidbody.velocity.x * 0.9f, myRigidbody.velocity.y, myRigidbody.velocity.z * 0.9f);
            
        }
    }

    void Jump()
    {
        myRigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
    }
}
